package ExcepcionPropia;

public class BadPostalCodeException extends Exception {

    public BadPostalCodeException() {
    }

    public BadPostalCodeException(String message) {
        super(message);
    }
}
