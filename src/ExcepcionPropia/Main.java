package ExcepcionPropia;

import javax.swing.*;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {

        String codigoPostal = JOptionPane.showInputDialog("Digite el código postal");

        try {
            comprobarLongitud(codigoPostal);
            throw new IOException();
        } catch (BadPostalCodeException|IOException ex) {
            System.out.println(ex.getMessage());
        }

        System.out.println(codigoPostal);

    }

    public static void comprobarLongitud(String codigoPostal) throws BadPostalCodeException {
        if(codigoPostal.length() != 5) {
            throw new BadPostalCodeException("El código postal debe contener 5 caracteres");
        }
    }
}
