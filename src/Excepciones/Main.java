package Excepciones;

import javax.swing.*;

public class Main {

    public static void main(String[] args) {

        boolean errores = true;
        do {
            try {
                double numerador = Double.parseDouble(JOptionPane.showInputDialog("Digite el numerador"));
                double denominador = Double.parseDouble(JOptionPane.showInputDialog("Digite el denominador"));

                System.out.println("El resultado es: " + dividir(numerador, denominador));
                errores = false;
            } catch (NumberFormatException nfe) {
                JOptionPane.showMessageDialog(null, "Debe digitar números");
            } catch (ArithmeticException ae) {
                JOptionPane.showMessageDialog(null, "El denominador no puede ser 0");
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Se produjo un error diferente a los anteriores");
                JOptionPane.showMessageDialog(null, ex.getMessage());
            } finally {
                JOptionPane.showMessageDialog(null, "Gracias por utilizar el programa");
            }
        } while (errores);
    }

    public static double dividir(double numero1, double numero2) throws ArithmeticException, NumberFormatException {
        return numero1 / numero2;
    }
}
