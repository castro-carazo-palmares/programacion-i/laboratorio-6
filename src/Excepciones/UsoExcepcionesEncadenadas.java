package Excepciones;

public class UsoExcepcionesEncadenadas {

    public static void main(String[] args) {
        try {
            metodo1();
        } catch (Exception exception) {
            System.out.println("Excepción lanzada en el main");
        }
    }

    public static void metodo1() throws Exception {
        try {
            metodo2();
        } catch (Exception exception) {
            System.out.println("Excepción lanzada en el método 1");
        }
    }

    public static void metodo2() throws Exception {
        throw new Exception();
    }

}
