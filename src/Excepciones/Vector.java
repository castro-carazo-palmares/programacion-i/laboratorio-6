package Excepciones;

import javax.swing.*;

public class Vector {

    public static void main(String[] args) {

        int[] vector = new int[10];

        int indice;
        try {
            indice = Integer.parseInt(JOptionPane.showInputDialog("Digite el índice"));
            vector[indice] = 5;
        } catch (ArrayIndexOutOfBoundsException ex) {
            JOptionPane.showMessageDialog(null,"El índice sobrepasa el límite del vector");
        }


    }

}
